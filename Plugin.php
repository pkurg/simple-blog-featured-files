<?php

namespace Pkurg\BlogFeaturedFiles;

use File;
use RainLab\Blog\Controllers\Posts as PostsController;
use RainLab\Blog\Models\Post as PostModel;
use System\Classes\PluginBase;

/**
 * Blog Plugin Information File
 */
class Plugin extends PluginBase
{

    public $require = ['RainLab.Blog'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'Blog Featured Files',
            'description' => 'BlogFeaturedFiles',
            'author' => 'Pkurg',
            'icon' => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

        PostModel::extend(function ($model) {
            $model->attachMany = [

                'featured_files' => 'System\Models\File',
                'featured_images' => 'System\Models\File',

            ];

        });

        PostsController::extendFormFields(function ($form, $model, $context) {

            if (!$model instanceof PostModel) {
                return;
            }

            // if (!$model->exists) {
            //     return;
            // }

            if ($form->isNested === false) {

                $form->addSecondaryTabFields([
                    'featured_files' => [
                        'label' => 'files',
                        'tab' => 'Files',
                        'type' => 'fileupload',
                        'mode' => 'file',

                    ],

                ]);

            }

        });
    }

}
